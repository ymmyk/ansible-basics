ansible-playbook-scaffolding
=========

This repos holds a skeleton/scaffolding structure for creating Ansible playbooks based on the [recommended directory layout](http://docs.ansible.com/ansible/latest/user_guide/playbooks_best_practices.html#directory-layout). To use this just clone this repo and name it to whatever you want, remove the .git folder, and start creating your playbook.


Prerequisites
-------------
* AWS CLI setup (via ~/.aws/config)
* Python 3.6+ (Preferably Python 3.8)


Installing
----------

* Install pipenv - `pip install pipenv`
* Install python libraries - `pipenv sync --dev`
* Configure groups_vars/all/base.yaml for correct AWS Region

Using
--------

```
# Initialize the python virtualenv
pipenv shell

# Execute the security_groups.yml
ansible-playbook security_groups.yml
```
